# Create a REST API to interact with the actual database:

This is a Flask app that exposes a REST API for keeping track of computers owned by a company.

Just make one table in your database: Computer. For every computer, we’ll need to know:

- Hard drive type
- Processor
- Amount of RAM
- Maximum RAM
- Hard drive space
- Form-factor (mini, desktop, etc.)

Note that some fields can only allow certain values. This is often called enumeration.

This app exposes some REST API endpoints, where the content type is in JSON.

The app performs the following operations (which are exposed at well-named URLs):

- List all computers
- Add a computer
- Edit a computer
- Delete a computer

# How to run this project:

1. **Create your virtual environment:**

    In the Ubuntu terminal-
    ```sh
    pipenv install
    ```

    For Windows-
    ```cmd
    python -m venv <name_of_your_virtual_env>
    ```

2. **Activate your virtual environment:**
    ```sh
    pipenv shell
    ```
    For Windows-
    ```cmd
    .\<name_of_your_virtual_env>\Scripts\activate
    ```

3. **Install the packages in the development mode.**
    ```sh
    python setup.py develop
    ```

4. **Install all your dependencies to your virtual environment:**
    In Ubuntu terminal use:
    ```sh
    pipenv install -r requirements.txt
    ```

    For Windows-
    ```cmd
    pip install -r requirements.txt
    ```

5. **Add your secret environmental variables, e.g., create 'setup_vars.sh':**
    For macOS/Linux, run:
    ```sh
    #!/bin/sh

    export DB_USER=
    export DB_PASS=
    export DB_HOST=
    export DB_NAME=
    export DB_PORT=
    ```

    For permissions, run:
    ```sh
    chmod +x setup_vars.sh
    ```
    Then run it:
    ```sh
    ./setup_vars.sh
    ```

    For Windows, open the PowerShell terminal, and run:
    ```powershell
    $env:DB_USER = ""
    $env:DB_PASS = ""
    $env:DB_HOST = ""
    $env:DB_NAME = ""
    $env:DB_PORT = #
    ```

6. **Download and pull your Docker image:**
    For macOS/Linux/PowerShell:
    ```sh
    docker-compose up -d
    ```

7. **Run the `app.py`:**
    ```sh
    python flask_app/app.py
    ```
    Then, copy the `<hostname_address>`

8. **Open another terminal, repeat steps 2 and 5.**

9. **Run the operations using cURL commands:**
    - To get the computers:
        ```sh
        curl -X GET <hostname_address>/computers
        ```
        e.g.
        ```sh
        curl -X GET http://localhost:5000/computers
        ```

        If there are more than five computers, from the above 'GET' cURL command, add the next page number to get more data of computers, e.g.
        ```sh
        curl -X GET http://localhost:5000/computers?page=2
        ```

    - Add a computer:
        ```sh
        curl -X POST -H "Content-Type: application/json" -d '<properties_of_computers>' -H 'Content-Type: application/json' <hostname_address>/computers
        ```
        `<properties_of_your_computer>` is in JSON, e.g.
        ```json
        {
            "form_factor": "mini",
            "hard_drive_space": "256 GB",
            "hard_drive_type": "HDD",
            "max_ram": "16 GB",
            "processor": "intel i5",
            "ram_amount": "8 GB"
        }
        ```
        e.g.
        ```sh
        curl -X POST -H "Content-Type: application/json" -d '{"hard_drive_type": "SSD", "processor": "Intel i9", "ram_amount": "32 GB", "max_ram": "64 GB", "hard_drive_space": "1000 GB", "form_factor": "Desktop"}' http://localhost:5000/computers
        ```

    - Modify a computer:
        ```sh
        curl -X PUT -H 'Content-Type: application/json' -d '<properties_of_your_computer>' <hostname_address>/computers/<computer_id_to_be_edited>
        ```
        `<properties_of_your_computer>` is in JSON, e.g:
        ```json
        {
            "form_factor": "mini",
            "hard_drive_space": "256 GB",
            "hard_drive_type": "HDD",
            "max_ram": "8 GB",
            "processor": "intel i7",
            "ram_amount": "8 GB"
        }
        ```
        e.g.
        ```sh
        curl -X PUT -H 'Content-Type: application/json' -d '{"form_factor": "Laptop", "ram_amount": "16 GB", "max_ram": "32 GB"}' http://localhost:5000/computers/1
        ```

    - Delete a computer:
        ```sh
        curl -X DELETE <hostname_address>/computers/<computer_id_to_be_deleted>
        ```
        e.g.
        ```sh
        curl -X DELETE http://localhost:5000/computers/1
        ```

10. **To stop the app, type:**
    ```sh
    Ctrl+C
    ```

11. **Stop and remove the Docker image:**
    ```sh
    docker-compose down
    ```

12. **Run the tests:**
    ```sh
    pytest tests/test_app.py
    ```

- [x] These steps were performed successfully.
