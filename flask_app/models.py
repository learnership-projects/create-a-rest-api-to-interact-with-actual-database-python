from enum import Enum

from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class HardDriveChoices(str, Enum):
    SSD = "SSD"
    HDD = "HDD"


class RamAmount(str, Enum):
    GB_4 = "4 GB"
    GB_8 = "8 GB"
    GB_16 = "16 GB"
    GB_32 = "32 GB"
    GB_64 = "64 GB"
    GB_126 = "126 GB"


class MaxAmount(str, Enum):
    GB_4 = "4 GB"
    GB_8 = "8 GB"
    GB_16 = "16 GB"
    GB_32 = "32 GB"
    GB_64 = "64 GB"
    GB_126 = "126 GB"


class HardDriveSpace(str, Enum):
    GB_126 = "126 GB"
    GB_246 = "256 GB"
    GB_512 = "512 GB"
    TB = "1000 GB"
    TB_2 = "2000 GB"
    TB_3 = "3000 GB"


class FormFactor(str, Enum):
    MINI = "Mini"
    MINI_ATX = "Mini-atx"
    DESKTOP = "Desktop"
    LAPTOP = "Laptop"


class Computer(db.Model):
    __tablename__ = "Computer"

    id = db.Column(db.Integer, primary_key=True)
    hard_drive_type = db.Column(db.Enum(HardDriveChoices), nullable=False)
    processor = db.Column(db.String(50), nullable=False)
    ram_amount = db.Column(db.Enum(RamAmount), nullable=False)
    max_ram = db.Column(db.Enum(MaxAmount), nullable=False)
    hard_drive_space = db.Column(db.Enum(HardDriveSpace), nullable=False)
    form_factor = db.Column(db.Enum(FormFactor), nullable=False)

    def __repr__(self) -> str:
        return f"(id='{self.id}', hard drive type='{self.hard_drive_type}', processor='{self.processor}', ram amount='{self.ram_amount}' and maximum ram='{self.max_ram}' has a hard drive space={self.hard_drive_space} and form factor={self.form_factor})"
