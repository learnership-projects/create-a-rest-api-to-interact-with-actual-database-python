from flask import Flask, jsonify, request, make_response

from flask_app.models import (
    Computer,
    db,
    HardDriveChoices,
    FormFactor,
    RamAmount,
    MaxAmount,
    HardDriveSpace,
)
from flask_app.config import Config


COMPUTERS_PER_PAGE = 5


def check_id_in_computers(input_id):
    computers = Computer.query.all()
    computer_ids = [computer.id for computer in computers]
    if not input_id in computer_ids:
        error_message = {
            "Error": "Computer not found!",
            "Message": "Invalid computer id. Please provide correct matching id of the computer to perform this operation.",
        }

        return make_response(jsonify(error_message), 404)


def get_all_computers():
    page = request.args.get("page", 1, type=int)
    computers = Computer.query.paginate(page=page, per_page=COMPUTERS_PER_PAGE)

    list_of_computers = []

    if computers:
        for computer in computers:
            computer_retrieved = {
                "id": computer.id,
                "hard drive type": computer.hard_drive_type.value,
                "processor": computer.processor,
                "ram amount": computer.ram_amount.value,
                "max ram": computer.max_ram.value,
                "hard drive space": computer.hard_drive_space.value,
                "form factor": computer.form_factor.value,
            }
            list_of_computers.append(computer_retrieved)

    return jsonify(list_of_computers)


def add_computer():
    data = request.json

    added_computer = {
        "hard_drive_type": HardDriveChoices(data["hard_drive_type"].upper()).name,
        "processor": data["processor"],
        "ram_amount": RamAmount(data["ram_amount"]).name,
        "max_ram": MaxAmount(data["max_ram"]).name,
        "hard_drive_space": HardDriveSpace(data["hard_drive_space"]).name,
        "form_factor": FormFactor(data["form_factor"].capitalize()).name,
    }
    computer = Computer(**added_computer)
    db.session.add(computer)
    db.session.commit()

    success_message = {"Message": "Computer added successfully"}
    return make_response(jsonify(success_message), 200)


def modify_computer(computer_id):
    checking_computer_id_response = check_id_in_computers(computer_id)

    if (
        checking_computer_id_response
        and checking_computer_id_response.status_code == 404
    ):
        return make_response(checking_computer_id_response.json, 404)

    data = request.json

    if not data:
        error_message = {"Error": "No computer property to modify."}
        return make_response(jsonify(error_message), 400)

    computer = db.session.query(Computer).filter_by(id=computer_id).first()

    if "hard_drive_type" in data:
        computer.hard_drive_type = HardDriveChoices(
            data["hard_drive_type"].upper()
        ).name
    if "processor" in data:
        computer.processor = data["processor"]
    if "ram_amount" in data:
        computer.ram_amount = RamAmount(data["ram_amount"]).name
    if "max_ram" in data:
        computer.max_ram = MaxAmount(data["max_ram"]).name
    if "hard_drive_space" in data:
        computer.hard_drive_space = HardDriveSpace(data["hard_drive_space"]).name
    if "form_factor" in data:
        computer.form_factor = FormFactor(data["form_factor"].capitalize()).name
    db.session.commit()

    success_message = {"Message": "Computer modified successfully"}
    return make_response(jsonify(success_message), 200)


def remove_computer(computer_id):
    checking_computer_id_response = check_id_in_computers(computer_id)

    if (
        checking_computer_id_response
        and checking_computer_id_response.status_code == 404
    ):
        return make_response(checking_computer_id_response.json, 404)

    computer = db.session.query(Computer).filter_by(id=computer_id).first()

    db.session.delete(computer)
    db.session.commit()

    success_message = {"Message": "Computer removed successfully"}
    return make_response(jsonify(success_message), 200)


def create_app() -> Flask:
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)

    with app.app_context():
        db.create_all()
    app.add_url_rule("/computers", view_func=get_all_computers, methods=["GET"])
    app.add_url_rule("/computers", view_func=add_computer, methods=["POST"])
    app.add_url_rule(
        "/computers/<int:computer_id>", view_func=modify_computer, methods=["PUT"]
    )
    app.add_url_rule(
        "/computers/<int:computer_id>", view_func=remove_computer, methods=["DELETE"]
    )

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
