#!/usr/bin/env python

import os


class Config:
    DEBUG = True
    DEVELOPMENT = True
    DB_HOST = os.getenv("DB_HOST", None)
    DB_PORT = os.environ.get("DB_PORT", 5432)
    DB_NAME = os.getenv("DB_NAME", None)
    DB_USER = os.getenv("DB_USER", None)
    DB_PASS = os.getenv("DB_PASS")
    SQLALCHEMY_DATABASE_URI = (
        f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False
