from setuptools import setup, find_packages

setup(
    name="flask_app",
    version="1.0.0",
    decription="This is the package that contains flask app that exposes a REST API for keeping track of computers owned by a company.",
    author="Kholofelo Moropane",
    packages=find_packages(include=["app", "models", "test_app"]),
)