# Documentation of production database
___

## What's the purpose of this project:
This is a Flask app project that exposes a REST API for keeping track of computers owned by company.

There is one table in our database: Computer.

Columns of this table are:
1. Hard drive type: This column is string data type and only available form factors are: "SSD" & "HDD",
2. Processor: This column is string data type,
3. Amount of ram: This column is an integer data type,
4. Maximum ram: This column is an integer data type,
5. Hard drive space: This column is an integer data type,
6. Form-factor (mini, mini,etc.): This column is string data type and only available form factors are: "Mini", "Mini-atx", "Desktop" & "Laptop".


The following operations(which are exposed at well named urls) are performed within the 'app.py':

1. List all computers in a function called `list_all_computers`, this function only shows a maximum number of five computers and if you want to get the remaining computers use the next page URL link.
2. Add a computer in a function called `add_computer`.
3. Edit a computer in a function called `modify_computer`.
4. Delete a computer in a function called `remove_computer`.
