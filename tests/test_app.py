import unittest
from unittest.mock import patch
from flask import json

from flask_app.app import (
    create_app,
    db,
    HardDriveChoices,
    RamAmount,
    MaxAmount,
    FormFactor,
    HardDriveSpace,
)
from flask_app.models import Computer
from flask_app.config import Config


class TestApp(unittest.TestCase):
    def setUp(self):
        Config.SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
        Config.TESTING = True
        self.app = create_app()
        self.client = self.app.test_client()

        with self.app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def test_add_computer_computer_added_success(self):
        computer_data_1 = {
            "hard_drive_type": "SSD",
            "processor": "Intel i7",
            "ram_amount": "16 GB",
            "max_ram": "32 GB",
            "hard_drive_space": "512 GB",
            "form_factor": "Laptop",
        }

        with self.app.app_context():
            with patch("flask_app.app.db.session") as mock_db_session:
                response = self.client.post("/computers", json=computer_data_1)

                computer_data_1["id"] = 1

                self.assertEqual(response.status_code, 200)
                self.assertEqual(
                    json.loads(response.data),
                    {"Message": "Computer added successfully"},
                )

                mock_db_session.add.assert_called_once()

    def test_get_all_computers(self):
        computer_data_1 = {
            "id": 1,
            "hard_drive_type": HardDriveChoices.SSD,
            "processor": "Intel i7",
            "ram_amount": RamAmount.GB_16,
            "max_ram": MaxAmount.GB_32,
            "hard_drive_space": HardDriveSpace.GB_512,
            "form_factor": FormFactor.LAPTOP,
        }

        with self.app.app_context():
            with patch("flask_app.app.Computer.query") as mock_computer_query:
                mock_paginate = mock_computer_query.paginate
                mock_paginate.return_value = [Computer(**computer_data_1)]

                response = self.client.get("/computers")

                self.assertEqual(response.status_code, 200)

                mock_paginate.assert_called_once_with(page=1, per_page=5)
                self.assertEqual(
                    [
                        {
                            "id": computer_data_1["id"],
                            "hard drive type": computer_data_1["hard_drive_type"],
                            "processor": computer_data_1["processor"],
                            "ram amount": computer_data_1["ram_amount"],
                            "max ram": computer_data_1["max_ram"],
                            "hard drive space": computer_data_1["hard_drive_space"],
                            "form factor": computer_data_1["form_factor"],
                        }
                    ],
                    response.json,
                )

    def test_modify_computer_endpoint(self):
        response = self.client.post(
            "/computers",
            json={
                "hard_drive_type": "SSD",
                "processor": "Intel Core i7",
                "ram_amount": "8 GB",
                "max_ram": "16 GB",
                "hard_drive_space": "256 GB",
                "form_factor": "Mini-atx",
            },
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.put("/computers/1", json={"processor": "Intel Core i9"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.data), {"Message": "Computer modified successfully"}
        )

        with self.app.app_context():
            computer = db.session.query(Computer).filter_by(id=1).first()
            self.assertEqual(computer.processor, "Intel Core i9")

    def test_remove_computer_endpoint(self):
        with self.app.app_context():
            with patch("flask_app.app.db.session.delete") as mock_delete:
                response = self.client.post(
                    "/computers",
                    json={
                        "hard_drive_type": "SSD",
                        "processor": "Intel Core i7",
                        "ram_amount": "8 GB",
                        "max_ram": "16 GB",
                        "hard_drive_space": "256 GB",
                        "form_factor": "Mini-atx",
                    },
                )
                self.assertEqual(response.status_code, 200)

                response = self.client.delete("/computers/1")
                self.assertEqual(response.status_code, 200)
                self.assertEqual(
                    json.loads(response.data),
                    {"Message": "Computer removed successfully"},
                )

                mock_delete.assert_called_once()


if __name__ == "__main__":
    unittest.main()
